# -*- coding: utf-8 -*-
from wx import TextCtrl, EVT_SET_FOCUS, EVT_KILL_FOCUS, BLACK, LIGHT_GREY


class PlaceholderTextCtrl(TextCtrl):
    def __init__(self, *args, **kwargs):
        self.default_text = kwargs.pop("placeholder", "")
        TextCtrl.__init__(self, *args, **kwargs)
        self.OnKillFocus(None)
        self.Bind(EVT_SET_FOCUS, self.OnFocus)
        self.Bind(EVT_KILL_FOCUS, self.OnKillFocus)
        if kwargs.get('value', False):
            self.default = False
        else:
            self.default = True

    def OnFocus(self, evt):
        self.SetForegroundColour(BLACK)
        if self.default:
            self.SetValue("")
        evt.Skip()

    def OnKillFocus(self, evt):
        if self.GetValue().strip() == "":
            self.SetValue(self.default_text)
            self.SetForegroundColour(LIGHT_GREY)
            self.default = True
        else:
            self.default = False
        if evt:
            evt.Skip()

    def GetRealValue(self):
        if self.default:
            return ""
        else:
            return self.GetValue()

    def SetValue(self, value):
        super(PlaceholderTextCtrl, self).SetValue(value)
        self.default = False
        self.SetForegroundColour(BLACK)
